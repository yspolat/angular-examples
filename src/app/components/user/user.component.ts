import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name:String;
  age:number;
  address:Address;
  // Array
  hobbies:string[];
  // It can be anythin - boolean,string,integer etc.
  hello:any;
  posts:Post;
  favorite:boolean;


  constructor(private dataService:DataService) {
    console.log('constructor run');
  }

  ngOnInit() {
    console.log('ngOnInit run')
    this.name = 'John Doe';
    this.age = 18;
    this.address = {
      street: '50 Main st',
      city: 'Boston',
      state: 'MA'
    }
    this.hobbies = ['Write code', 'Listen to music'];
    this.hello = 1;
    this.dataService.getPosts().subscribe((posts) => {
      console.log(posts);
    });
    this.favorite=false;
  }

  // onClick function for user.component.html button
  onClick(){
    //console.log('Hello');

    // Manipulating properties
    this.name='Yavuz Selim Polat';
    this.hobbies.push('Watching movie');
  }

  addHobby(hobby) {
    console.log(hobby);
    // Push begin instead of end
    this.hobbies.unshift(hobby);
    return false;
  }

  deleteHobby(hobby){
    console.log(hobby);

    for (let i=0; i<this.hobbies.length; i++){
      if (this.hobbies[i] == hobby){
        this.hobbies.splice(i,1);
      }
    }

  }

  toggleFavorite(){
    console.log("toggling");
    this.favorite=!this.favorite;
  }
}


interface Address {
  street:string,
  city:string,
  state:string
}

interface Post {
  id:number,
  title:string,
  body:string,
  userId:number
}